s1=set()
# add elements in set
s1.add(1)
s1.add(1)
s1.add(2)
s1.add(3)
s1.add(3)

s2={1,2,3,4,5,6}
s3={5,6,7,8,9,10}
# evns=set()
# for i in range(0,100,2):
#     evns.add(i)
# print(evns)

# union of set
# u=s1.union(s2)
# u=s2.union(s3)
# u=s1|s2
# print('union:',u)

# intersection of set
# ints=s2.intersection(s3)
# ints=s2&s3
# print('intersection:',ints)

# difference of set
# d=s2.difference(s3)
d=s2-s3
print('difference:',d)

# membership
print(100 in s2)
print(2 in s2)
print(100 not in s2)
print(2 not in s2)
print(s2>s3)