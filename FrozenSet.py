st=set([1,2,3,4])
st.add(5)                           #mutable set
print(st)

fs=frozenset([1,2,3,4,5])
# fs.add(6)                                    #immutable set i.e. it can't be add elements or update
print(fs)

for i in fs:
    print(i)